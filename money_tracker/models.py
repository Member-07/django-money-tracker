from django.db import models

# Create your models here.
class Income(models.Model):
    name = models.CharField(max_length=60)
    total = models.DecimalField(max_digits=15, decimal_places=2)
    category_id = models.IntegerField()
    def __str__(self):
        return self.name

class Categorty(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name