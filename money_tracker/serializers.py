from rest_framework import serializers
from .models import Income

class MoneyTrackerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Income
        fields = ["name", "total", "category_id"]