# Generated by Django 4.1.3 on 2022-11-09 10:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('money_tracker', '0002_alter_income_total'),
    ]

    operations = [
        migrations.RenameField(
            model_name='income',
            old_name='catalog_id',
            new_name='category_id',
        ),
    ]
